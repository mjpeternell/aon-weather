module.exports = function (grunt) {

// Project configuration
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            options: {
                stripBanners: true,
                banner: '/*! <%= pkg.title %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %>\n' +
                        ' * <%= pkg.homepage %>\n' +
                        ' * Copyright (c) <%= grunt.template.today("yyyy") %>;' +
                        ' * Licensed GPLv2+' +
                        ' */\n'
            },
            wp_pixelfire_theme: {
                src: ['public_html/js/src/main.js'],
                dest: 'public_html/js/main.js'
            },
            wp_pixelfire_theme_1: {
                src: ['public_html/js/src/icon-animation.js'],
                dest: 'public_html/js/icon-animation.js'
            }
        },
//        shell: {
//            options: {
//                stderr: false
//            },
//            target: {
//                command: 'cp -a ~/Sites/rooster/wp-content/rooster-park-theme-source/release/wp_pixelfire_theme ~/Sites/rooster/wp-content/themes/'
//            }
//        },
        jshint: {
            all: [
                'Gruntfile.js',
                'public_html/js/src/**/*.js'
                //'test/**/*.js'
            ],
            options: {
                curly: true,
                eqeqeq: true,
                immed: true,
                latedef: true,
                newcap: true,
                noarg: true,
                sub: true,
                undef: true,
                boss: true,
                eqnull: true,
                globals: {
                    exports: true,
                    module: false,
                    $: false,
                    jQuery: false,
                    console: false,
                    document: false,
                    window: false,
                    alert: false,
                    google: false,
                    setInterval: false,
                    setTimeout: false,
                    moment: false,
                    self: false,
                    Image:false,
                    clearInterval: false
                }
            }
        },
        uglify: {
            options: {
                mangle: false,
                separator: '\n\n',
                report: 'min',
                banner: '/*! <%= pkg.title %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %>\n' +
                        ' * <%= pkg.homepage %>\n' +
                        ' * Copyright (c) <%= grunt.template.today("yyyy") %>;' +
                        ' * Licensed GPLv2+' +
                        ' */\n'
            },
            my_target_1: {
                files: {
                    'public_html/js/main.min.js': ['public_html/js/main.js']
                }
            },
            my_target_2: {
                files: {
                    'public_html/js/icon-animation.min.js': ['public_html/js/icon-animation.js']
                }
            }
        },
        test: {
            files: ['/test/**/*.js']
        },
        sass: {
            all: {
                files: {
                    'public_html/css/style.css': 'public_html/sass/style.scss'
                }
            }
        },
        cssmin: {
            options: {
                shorthandCompacting: false,
                roundingPrecision: -1,
                banner: '/*! <%= pkg.title %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %>\n' +
                        ' * <%= pkg.homepage %>\n' +
                        ' * Copyright (c) <%= grunt.template.today("yyyy") %>;' +
                        ' * Licensed GPLv2+' +
                        ' */\n'
            },
            target: {
                expand: true,
                cwd: 'public_html/css/',
                src: ['*.css', '!*.min.css'],
                dest: 'public_html/css/',
                ext: '.min.css'
            }
        },
        watch: {
            sass: {
                files: ['public_html/sass/**/*.scss'],
                tasks: ['sassy'],
                options: {
                    debounceDelay: 500
                }
            },
            scripts: {
                files: ['public_html/js/src/**/*.js'],
                tasks: ['jshint', 'concat', 'uglify'],
                options: {
                    debounceDelay: 500
                }
            }
        },
        clean: {
            main: ['release/<%= pkg.name %>']
        },
        copy: {
            // Copy the plugin to a versioned release directory
            main: {
                src: [
                    '**',
                    '!bower_components/**',
                    '!node_modules/**',
                    '!nbproject/**',
                    '!release/**',
                    '!.git/**',
                    '!css/src/**',
                    '!js/src/**',
                    '!img/src/**',
                    '!config.rb',
                    '!Gruntfile.js',
                    '!package.json',
                    '!.gitignore',
                    '!.gitmodules'
                ],
                dest: 'release/<%= pkg.name %>/'
            }
        },
        compress: {
            main: {
                options: {
                    mode: 'zip',
                    archive: './release/<%= pkg.name %>.<%= pkg.version %>.zip'
                },
                expand: true,
                cwd: 'release/<%= pkg.name %>/',
                src: ['**/*'],
                dest: '<%= pkg.name %>/'
            }
        }
    });
    // Load other tasks
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-compress');
    grunt.loadNpmTasks('grunt-shell');
    // Default task.
    grunt.registerTask('default', ['jshint', 'concat', 'uglify', 'sass', 'cssmin']);
    //Add 'shell' to 'Build" after building theme from grunt-init
    grunt.registerTask('build', ['default', 'clean', 'copy', 'compress']);
    grunt.registerTask('sassy', ['sass', 'cssmin']);
    grunt.util.linefeed = '\n';
};