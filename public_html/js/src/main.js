/*!  - v1.0.0 - 2018-02-05
 *
 * Copyright (c) 2018; * Licensed GPLv2+ */
function remove_for_wx() {
    $('#seven-day-forecast-list').html('');
}

function remove_obs_wx() {
    $('#observations').html('');
}


var web_arr = [];
var wx_arr = [];
var data_web = "";
//load the image for the Sprite Animation
var img = new Image();
//load the image for the Sprite Animation

var icon_img = new Image();
//load the image for the Sprite Animation

// Set Forcast Icon Code Variable
var fx_icon_code = "xyz";

// Function to set the animation sprite sheet url based on observed weather icon code.
function set_fx_icon_url(fx_icon_code) {
    console.log("fx_icon_code: " + fx_icon_code);
    //icon_img.src = 'animations/'+ fx_icon_code +'.png';
}

function forcast_wx_data() {
    $.ajax({
        url: 'http://api.wunderground.com/api/34cc2269c4bdbdc8/forecast10day/q/IL/Chicago.json',
        type: 'post',
        success: function (data) {
            // Perform operation on return value
            var my_forcast = data.forecast.simpleforecast.forecastday;
            var my_time_stamp = data.forecast.txt_forecast.date;
            console.log("Chicago Forecast Weather Updated: " + my_time_stamp);

            var my_wx_count = -1;
            var my_icon_count = 0;

            for (var k in my_forcast) {
                my_wx_count++;
                my_icon_count++;
                wx_arr.push({"icon": my_forcast[k].icon});

                fx_icon_code = my_forcast[k].icon;
                if (my_wx_count === 0) {
                   continue;
               }
                if (my_wx_count === 6) {
                    break;
                }

                $('#seven-day-forecast-list').append(
                        '<div class="forecast-tombstone grid__1-column">' +
                        '<div class="tombstone-container">' +
                        '<div class="period-name"><span class="weekday-name">' + my_forcast[k].date.weekday + '</span><span class="month-name">' + my_forcast[k].date.monthname_short + '</span><span class="date-name">' + my_forcast[k].date.day + '</span></div>' +
                        '<span class="conditions">' + my_forcast[k].conditions + '</span>' +
                        '<span class="temp-data high">High: ' + my_forcast[k].high.fahrenheit + '&#176;F / ' + my_forcast[k].high.celsius + '&#176;C</span>' +
                        '<span class="temp-data low">Low: ' + my_forcast[k].low.fahrenheit + '&#176;F / ' + my_forcast[k].low.celsius + '&#176;C</span>' +
                        '<div id="fx-Icon-' + my_icon_count + '" class="weather-icon icon-' + my_forcast[k].icon + '">' +
                        '</div>' +
                        //'<span class="short-forecast-code">Icon Code: ' + my_forcast[k].icon + '</span>' +
                        //'<span class="short-forecast-code">Icon Image: sheet_'+ my_forcast[k].icon + '</span>' +
                        '</div></div>'
                        );

            }
        },
        error: function (data, status, xhr) {
            console.log("WU forecast 10-day API XHR Status: " + xhr.status);
        },
        complete: function (data, status) {
            console.log("WU forecast 10-day API Status: " + status);

            set_fx_icon_url(fx_icon_code);
            setTimeout(function () {
                remove_for_wx();
                forcast_wx_data();
            }, 1800000);
        }
    });
}

// Set Icon Code Variable
var icon_code = "xyz";
// Set temp Code Variable
var temp_code = "";

// Set wind sped Code Variable
var wind_code = "";

// Function to set the animation sprite sheet url based on observed weather icon code.
function set_img_url(icon_code, temp_code, wind_code) {
    var your_players_ip_address = "192.168.20.117";

    $.ajax({
        type: "GET",
        url: "http://" + your_players_ip_address + ":17236/services;execute?command=start%20playlist%20-o%20-n%20\"" + icon_code + temp_code + "\"",
        dataType: "jsonp",
        success: function(data) {
            // success handler
            //$('#my_video_url').html(this.url);
            console.log(this.url);
        }, error: function(jqXHR, textStatus, errorThrown) {
            // error handler
        }
    });
}


function observed_wx_data() {
    $.ajax({
        url: 'http://api.wunderground.com/api/34cc2269c4bdbdc8/conditions/q/IL/Chicago.json',
        type: 'post',
        success: function (data) {
            // Perform operation on return value
            var my_obs_wx_data = data.current_observation;
            var my_obs_time_stamp = data.current_observation.observation_time;
            console.log("Chicago Observered Weather Updated: " + my_obs_time_stamp);

            var dateVal = my_obs_wx_data.observation_epoch;
            var date_mom = moment.unix(dateVal);
            var date_clean = date_mom.tz("America/Chicago").format('dddd D');
            icon_code = my_obs_wx_data.icon;
            var wind_mph = Math.ceil(my_obs_wx_data.wind_mph);
            //var wind_mph = Math.ceil(25);
            var my_temp_f = Math.floor(my_obs_wx_data.temp_f);
            var my_temp_c = Math.floor(my_obs_wx_data.temp_c);
            console.log("wind_mph: " + wind_mph);
            console.log("icon code: " + icon_code);

            if (my_temp_f >= 60) {
                temp_code = "_warm";
            } else if (my_temp_f <= 59) {
                temp_code = "_cold";
            } else {
                temp_code = "";
            }

            if (wind_mph >= 25) {
                wind_code = "_windy";
                icon_code = "windy";
            } else {
                wind_code = "_no_wind";
            }

            if (icon_code === "chanceflurries" || icon_code === "chancerain" || icon_code === "chancesleet" || icon_code === "chancesnow" || icon_code === "chancetstorms" || icon_code === "flurries" || icon_code === "fog" || icon_code === "hazy" || icon_code === "sleet" || icon_code === "snow" || icon_code === "tstorms" || icon_code === "rain"  || icon_code === "unknown"   ) {
                temp_code = "";
                wind_code = "";
            }

            $('#observations').append(
                    '<li class="obs-date">' + date_clean + '</li>' +
                    '<li class="obs-temp"><span class="far">' + my_temp_f + '&deg;F</span> / <span class="cel">' +  my_temp_c + '&deg;C</span></li>' +
                    '<li class="obc-icon">' + my_obs_wx_data.weather + '</li>'
                    //'<li class="obs-icon">Icon Code For Models: ' + icon_code + '</li>' +
                    //'<li class="obs-icon">Icon Temp Code For Models: ' + temp_code + '</li>' +
                    //'<li class="obs-icon">Icon Wind Code: ' + wind_code + '</li>'
                    );
        },
        error: function (data, status, xhr) {
            console.log("WU Condition API XHR Status: " + xhr.status);
        },
        complete: function (data, status) {
            console.log("WU Conditions API Status: " + status);
            set_img_url(icon_code, temp_code, wind_code);
            setTimeout(function () {
                remove_obs_wx();
                observed_wx_data();
            }, 1800000);
        }
    });
}

var chiDate = null, millTime = null, chiTime = null, date = null;

var time_update = function () {

    var my_time_zone = 'America/Chicago';
    date = moment(new Date());
    //console.log(date.tz(my_time_zone).format('h:mm'));
    chiTime.html(date.tz(my_time_zone).format('h:mm')); //h:mm:ss

    chiDate.html(date.tz(my_time_zone).format('MMMM D, YYYY'));
    millTime.html(date.tz(my_time_zone).format('H'));

    var twenty_four = date.tz(my_time_zone).format('H');
    var current_flag = "";

    if (twenty_four < 11) {
        millTime.html('AM');
        current_flag = 0;
    }
    if (twenty_four > 11) {
        millTime.html('PM');
        current_flag = 1;
    }
    if (current_flag === 1) {
        $('.am').removeClass('current');
        $('.pm').addClass('current');
    }
    if (current_flag === 0) {
        $('.am').addClass('current');
        $('.pm').removeClass('current');
    }
};

$(document).ready(function () {
    forcast_wx_data();
    observed_wx_data();
    //setTimeout(fetchdata, 5000);
    chiDate = $('#chiDateA, #chiDateB');
    chiTime = $('#chiTimeA, #chiTimeB');
    millTime = $('#millTime');
    time_update();
    setInterval(time_update, 1000);

});
